package com.mirriad.supermarket.exercise.domain;

import com.mirriad.supermarket.exercise.app.calculation.SimpleCalculationRule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;

public class Checkout {

    private final List<Item> items;
    private final BigDecimal totalPrice;

    public Checkout(List<Item> items, BigDecimal totalPrice){
        this.items = items;
        this.totalPrice = totalPrice;
    }

    public List<Item> getItems() {
        return items;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public static Checkout EMPTY(){
        return new Checkout(new ArrayList(), ZERO);
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();

        String withoutPrice = items.stream()
                .map(item -> "" + item.getLabel() + " : " + item.getPrice())
                .collect(Collectors.joining("\n"));
        BigDecimal originalPrice = new SimpleCalculationRule().calculatePrice(ZERO, items).getCurrentPrice();

        return withoutPrice +
                "\nOriginal price : " + originalPrice.toString() +
                "\nTotal price : " + totalPrice.toString();
    }
}

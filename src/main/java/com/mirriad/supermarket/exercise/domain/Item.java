package com.mirriad.supermarket.exercise.domain;

import java.math.BigDecimal;

public class Item {

    private final BigDecimal price;
    private final String code;
    private final String label;

    public Item(BigDecimal price,  String code, String label){
        this.price = price;
        this.code = code;
        this.label = label;
    }

    public BigDecimal getPrice(){
        return this.price;
    }

    public String getCode(){
        return code;
    }

    public final String getLabel(){
        return this.label;
    }
}

package com.mirriad.supermarket.exercise.app.calculation;

import com.mirriad.supermarket.exercise.domain.Item;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;

public class BuyNItemsXGetKItemsYForFree implements CalculationRule {

    private final String paidItemsCode;
    private final String freeItemsCode;
    private final int nbPaidItems;
    private final int nbFreeItems;

    public BuyNItemsXGetKItemsYForFree(String paidItemsCode, String freeItemsCode, int nbPaidItems, int nbFreeItems){
        this.paidItemsCode = paidItemsCode;
        this.freeItemsCode = freeItemsCode;
        this.nbPaidItems = nbPaidItems;
        this.nbFreeItems = nbFreeItems;
    }

    @Override
    public CalculationStep calculatePrice(BigDecimal currentPrice, List<Item> items) {
        if (CollectionUtils.isEmpty(items)){
            return CalculationStep.EMPTY_STEP(currentPrice);
        }

        Map itemsByCode = items.stream()
                .collect(Collectors.groupingBy(i -> isItemAffected(i)));

        List<Item> excessItems = (List<Item>) itemsByCode.get(false);
        BigDecimal price = getTotalPrice((List<Item>) itemsByCode.get(true)).add(currentPrice);

        return new CalculationStep(price, excessItems);
    }

    private boolean isItemAffected(Item item){
        return item.getCode().equals(this.paidItemsCode) || item.getCode().equals(this.freeItemsCode);
    }

    private BigDecimal getTotalPrice(List<Item> items){
        if (items == null){
            return ZERO;
        }
        Map itemsXandY = items.stream()
                .collect(Collectors.groupingBy(i -> i.getCode().equals(paidItemsCode)));

        int nbFreeItems = getNumberFreeItemsY((List<Item>) itemsXandY.get(true));
        BigDecimal priceItemX = getSubPrice(((List<Item>) itemsXandY.get(true)), ZERO, 0);
        BigDecimal totalPrice = getSubPrice(((List<Item>) itemsXandY.get(false)), priceItemX, nbFreeItems);

        return totalPrice;
    }

    private int getNumberFreeItemsY(List<Item> itemsX){
        return (itemsX.size() / nbPaidItems) * nbFreeItems;
    }

    private BigDecimal getSubPrice(List<Item> items, BigDecimal originalPrice, int skip){
        return items.stream()
                .skip(skip)
                .map(i -> i.getPrice())
                .reduce(originalPrice, BigDecimal::add);
    }
}

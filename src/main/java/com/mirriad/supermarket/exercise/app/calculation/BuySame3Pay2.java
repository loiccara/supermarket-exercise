package com.mirriad.supermarket.exercise.app.calculation;

import com.google.common.math.IntMath;
import com.mirriad.supermarket.exercise.domain.Item;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;

public class BuySame3Pay2 implements CalculationRule {

    private final String itemCode;

    public BuySame3Pay2(String itemCode){
        this.itemCode = itemCode;
    }

    @Override
    public CalculationStep calculatePrice(BigDecimal currentPrice, List<Item> items) {
        if (CollectionUtils.isEmpty(items)){
            return CalculationStep.EMPTY_STEP(currentPrice);
        }

        Map productsByCode = items.stream()
                .collect(Collectors.groupingBy(i -> i.getCode().equals(this.itemCode)));

        List<Item> excessProducts = (List<Item>) productsByCode.get(false);
        BigDecimal price = getPrice((List<Item>) productsByCode.get(true)).add(currentPrice);

        return new CalculationStep(price, excessProducts);
    }

    private BigDecimal getPrice(List<Item> items){
        if (items == null){
            return ZERO;
        }

        BigDecimal unitCost = items.get(0).getPrice();
        int nbItems = getNbNonFreeItems(items.size());
        return unitCost.multiply(new BigDecimal(nbItems));
    }

    private int getNbNonFreeItems(int nbProducts){
        return nbProducts - IntMath.divide(nbProducts, 3, RoundingMode.DOWN);
    }
}

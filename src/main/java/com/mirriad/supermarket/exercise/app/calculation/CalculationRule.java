package com.mirriad.supermarket.exercise.app.calculation;

import com.mirriad.supermarket.exercise.domain.Item;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Collections.EMPTY_LIST;

public interface CalculationRule {

    default CalculationStep calculatePrice(BigDecimal currentCost, List<Item> items){
        if (CollectionUtils.isEmpty(items)){
            return CalculationStep.EMPTY_STEP(currentCost);
        }

        BigDecimal newCost = items.stream()
                .map(i -> i.getPrice())
                .reduce(currentCost, BigDecimal::add);

        return new CalculationStep(newCost, EMPTY_LIST);
    }
}

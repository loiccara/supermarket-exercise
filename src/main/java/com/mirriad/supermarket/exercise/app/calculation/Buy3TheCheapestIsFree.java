package com.mirriad.supermarket.exercise.app.calculation;

import com.google.common.math.IntMath;
import com.mirriad.supermarket.exercise.domain.Item;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ZERO;

public class Buy3TheCheapestIsFree implements CalculationRule {

    private final Set<String> codes;

    public Buy3TheCheapestIsFree(Set<String> codes){
        this.codes = codes;
    }

    @Override
    public CalculationStep calculatePrice(BigDecimal currentPrice, List<Item> items) {
        if (CollectionUtils.isEmpty(items)){
            return CalculationStep.EMPTY_STEP(currentPrice);
        }

        Map itemsByCode = items.stream()
                .collect(Collectors.groupingBy(
                        i -> codes.contains(i.getCode())
                ));

        List<Item> excessProducts = (List<Item>) itemsByCode.get(false);
        List<Item> selectedItems = ((List<Item>) itemsByCode.get(true)).stream()
                .sorted(Comparator.comparing(Item::getPrice).reversed())
                .collect(Collectors.toList());

        BigDecimal price = getPrice(currentPrice, selectedItems);

        return new CalculationStep(price, excessProducts);
    }

    private BigDecimal getPrice(BigDecimal currentPrice, List<Item> orderedItems) {
        if (orderedItems == null) {
            return ZERO;
        }

        BigDecimal subTotal;
        if (orderedItems.size() < 3) {
            subTotal = orderedItems.stream()
                    .map(i -> i.getPrice())
                    .reduce(currentPrice, BigDecimal::add);
        } else if (orderedItems.size() == 1) {
            // pick 2 expensive
            BigDecimal newPrice = orderedItems.get(0).getPrice();
            List newList = orderedItems.subList(2, Math.min(orderedItems.size(), 1));

            subTotal = getPrice(newPrice, newList);
        } else {
            // pick 2 expensive
            BigDecimal newPrice = orderedItems.subList(0, 2).stream()
                    .map(i -> i.getPrice())
                    .reduce(currentPrice, BigDecimal::add);
            List newList = orderedItems.subList(2, (orderedItems.size() - 1));

            subTotal = getPrice(newPrice, newList);
        }

        return subTotal;
    }
}

package com.mirriad.supermarket.exercise.app.calculation;

import com.mirriad.supermarket.exercise.domain.Item;

import java.math.BigDecimal;
import java.util.List;

import static java.util.Collections.EMPTY_LIST;

public class CalculationStep {
    private final BigDecimal currentPrice;
    private final List<Item> remainingItems;

    public CalculationStep(BigDecimal currentPrice, List<Item> remainingProducts){
        this.currentPrice = currentPrice;
        this.remainingItems = remainingProducts;
    }

    public static final CalculationStep EMPTY_STEP(BigDecimal currentCost){
        return new CalculationStep(currentCost, EMPTY_LIST);
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public List<Item> getRemainingProducts() {
        return remainingItems;
    }
}

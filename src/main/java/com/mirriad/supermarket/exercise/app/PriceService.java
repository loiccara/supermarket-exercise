package com.mirriad.supermarket.exercise.app;

import com.mirriad.supermarket.exercise.app.calculation.CalculationRule;
import com.mirriad.supermarket.exercise.app.calculation.CalculationStep;
import com.mirriad.supermarket.exercise.app.calculation.SimpleCalculationRule;
import com.mirriad.supermarket.exercise.domain.Checkout;
import com.mirriad.supermarket.exercise.domain.Item;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

import static com.mirriad.supermarket.exercise.domain.Checkout.*;
import static java.math.BigDecimal.ZERO;

public class PriceService {

    Checkout calculatePrice(List<Item> items, List<CalculationRule> rules){
        if (CollectionUtils.isEmpty(items)){
            return Checkout.EMPTY();
        }

        BigDecimal price = ZERO;
        List<Item> tempItems = items;

        for (CalculationRule rule : rules){
            CalculationStep step = rule.calculatePrice(price, tempItems);
            price = step.getCurrentPrice();
            tempItems = step.getRemainingProducts();
        }

        BigDecimal totalPrice = new SimpleCalculationRule().calculatePrice(price, tempItems).getCurrentPrice();
        return new Checkout(items, totalPrice);
    }
}

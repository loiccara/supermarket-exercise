package com.mirriad.supermarket.exercise.app.calculation

import com.mirriad.supermarket.exercise.domain.Item
import spock.lang.Specification
import spock.lang.Unroll

import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN

class BuyNItemsXGetKItemsYForFreeSpec extends Specification {


    @Unroll
    def "should not change the current price for an empty list of items"(){
        when:
        def step = new BuyNItemsXGetKItemsYForFree(
                "bathingDuck", "shinyThing",
                2, 1
        ).calculatePrice(TEN, items)

        then:
        step.currentPrice == TEN
        step.remainingProducts == []

        where:
        items << [[], null]
    }

    def "should return a remaining list of not concerned items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "shinyThing", "Shiny Thing"),
                new Item(ONE, "foo", "Foo"),
                new Item(ONE, "bar", "Bar"),
        ]

        when:
        def step = new BuyNItemsXGetKItemsYForFree(
                "bathingDuck", "shinyThing",
                2, 1
        ).calculatePrice(ONE, items)

        then:
        step.remainingProducts.size() == 2
    }

    def "should calculate a price for 1 item X and 1 item Y, none is free"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
        ]

        when:
        def step = new BuyNItemsXGetKItemsYForFree(
                "bathingDuck", "shinyThing",
                2, 1
        ).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(12)
    }

    def "should calculate a price for 2 items X and 1 Y, Y is free"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "shinyThing", "Shiny Thing")
        ]

        when:
        def step = new BuyNItemsXGetKItemsYForFree(
                "bathingDuck", "shinyThing",
                2, 1
        ).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(21)
    }

    def "should calculate a price for 2 X and 2 Y, get 1 Y free"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing")
        ]

        when:
        def step = new BuyNItemsXGetKItemsYForFree(
                "bathingDuck", "shinyThing",
                2, 1
        ).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(22)
    }

    def "should calculate a price for 7 X and 7 Y, get 3 Y free"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing"),
                new Item(ONE, "shinyThing", "Shiny Thing")
        ]

        when:
        def step = new BuyNItemsXGetKItemsYForFree(
                "bathingDuck", "shinyThing",
                2, 1
        ).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(75)
    }
}

package com.mirriad.supermarket.exercise.app.calculation

import com.mirriad.supermarket.exercise.domain.Item
import spock.lang.Specification
import spock.lang.Unroll

import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN

class Buy3TheCheapestIsFreeSpec extends Specification {

    @Unroll
    def "should not change the current price for an empty list of items"(){
        when:
        def step = new Buy3TheCheapestIsFree(
                ["bathingDuck", "shinyThing"] as Set
        ).calculatePrice(TEN, items)

        then:
        step.currentPrice == TEN
        step.remainingProducts == []

        where:
        items << [[], null]
    }

    def "should return a remaining list of not concerned items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "foo", "Foo"),
                new Item(ONE, "bar", "Bar"),
        ]

        when:
        def step = new Buy3TheCheapestIsFree(["bathingDuck", "shinyThing"] as Set).calculatePrice(ONE, items)

        then:
        step.remainingProducts.size() == 2
    }

    def "should calculate a price for 1 item"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new Buy3TheCheapestIsFree(["bathingDuck", "shinyThing"] as Set).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(11)
    }

    def "should calculate a price for 2 items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new Buy3TheCheapestIsFree(["bathingDuck", "shinyThing"] as Set).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(21)
    }

    def "should calculate a price for 3 items, get 1 free"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(TEN, "shinyThing", "Shiny Thing")
        ]

        when:
        def step = new Buy3TheCheapestIsFree(["bathingDuck", "shinyThing"] as Set).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(12)
    }

    def "should calculate a price for 7 items, get the 2 cheapest free"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(TEN, "shinyThing", "Shiny Thing"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(TEN, "shinyThing", "Shiny Thing"),
                new Item(TEN, "shinyThing", "Shiny Thing")
        ]

        when:
        def step = new Buy3TheCheapestIsFree(["bathingDuck", "shinyThing"] as Set).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(33)
    }
}

package com.mirriad.supermarket.exercise.app.calculation

import com.mirriad.supermarket.exercise.domain.Item
import spock.lang.Specification

import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.ZERO

class SimpleCalculationRuleSpec extends Specification {

    def "should not change the current price for an empty list of items"(){
        when:
        def step = new SimpleCalculationRule().calculatePrice(TEN, items)

        then:
        step.currentPrice == TEN
        step.remainingProducts == []

        where:
        items << [[], null]
    }

    def "should calculate the current price for the remaining items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "smallblackpen", "Small black pen"),
                new Item(new BigDecimal(100), "thatexpensivething", "Beautiful useless decoration")
        ]

        when:
        def step = new SimpleCalculationRule().calculatePrice(TEN, items)

        then:
        step.currentPrice == new BigDecimal(141);
        step.remainingProducts == []
    }
}

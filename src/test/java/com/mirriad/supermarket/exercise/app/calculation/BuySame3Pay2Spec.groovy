package com.mirriad.supermarket.exercise.app.calculation

import com.mirriad.supermarket.exercise.app.calculation.BuySame3Pay2
import com.mirriad.supermarket.exercise.domain.Item
import spock.lang.Specification
import spock.lang.Unroll

import static java.math.BigDecimal.*
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN

class BuySame3Pay2Spec extends Specification {

    @Unroll
    def "should not change the current price for an empty list of items"(){
        when:
        def step = new BuySame3Pay2("bathingDuck").calculatePrice(TEN, items)

        then:
        step.currentPrice == TEN
        step.remainingProducts == []

        where:
        items << [[], null]
    }

    def "should return a remaining list of not concerned items"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "foo", "Foo"),
                new Item(ONE, "bar", "Bar"),
        ]

        when:
        def step = new BuySame3Pay2("bathingDuck").calculatePrice(ONE, items)

        then:
        step.remainingProducts.size() == 2
    }

    def "should calculate a price 1 for 1 element with price 1"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new BuySame3Pay2("bathingDuck").calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(2)

    }

    def "should calculate a price 2 for 2 elements with price 1"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new BuySame3Pay2("bathingDuck").calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(3)
    }

    def "should calculate a price 2 for 3 elements with price 1, so get one free"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new BuySame3Pay2("bathingDuck").calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(3)
    }

    def "should calculate a price 3 for 4 elements with price 1, so get one free"(){
        given:
        def items = [
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck"),
                new Item(ONE, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new BuySame3Pay2("bathingDuck").calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(4)
    }
}

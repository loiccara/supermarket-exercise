package com.mirriad.supermarket.exercise.app

import com.mirriad.supermarket.exercise.app.calculation.Buy2ForSpecialPrice
import com.mirriad.supermarket.exercise.app.calculation.BuySame3Pay2
import com.mirriad.supermarket.exercise.domain.Checkout
import com.mirriad.supermarket.exercise.domain.Item
import org.hibernate.annotations.Check
import spock.lang.Specification

import static com.mirriad.supermarket.exercise.domain.Checkout.EMPTY
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN

class PriceServiceSpec extends Specification {

    private PriceService service

    def setup(){
        service = new PriceService()
    }

    def "should return a price = 0 for an empty list of items and rules"(){
        when:
        def checkout = service.calculatePrice(items, [])

        then:
        checkout.totalPrice == 0
        checkout.items == []

        where:
        items << [[], null]
    }

    def "should calculate the price on items without rules"(){
        given:
        def items = [
                new Item(BigDecimal.valueOf(1), "foo", "Foo"),
                new Item(BigDecimal.valueOf(18.9), "bar", "Bar"),
        ]

        expect:
        service.calculatePrice(items, []).totalPrice == 19.9
    }

    def "should apply Buy3pay2 then buy2ForSpecialPrice"(){
        given: "Rules"
        def rules =[
                new BuySame3Pay2("shinyThing"),
                new Buy2ForSpecialPrice("fluffyDuck", new BigDecimal(12))
        ]

        and: "Items"
        def items = [
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(ONE, "gum", "Some gum"),
                new Item(ONE, "gum", "Some gum"),
                new Item(ONE, "gum", "Some gum")
        ]

        expect:
        service.calculatePrice(items, rules).totalPrice == 349
    }
}


package com.mirriad.supermarket.exercise.app.calculation

import com.mirriad.supermarket.exercise.domain.Item
import spock.lang.Specification
import spock.lang.Unroll

import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN

class Buy2ForSpecialPriceSpec extends Specification {

    @Unroll
    def "should not change the current price for an empty list of items"(){
        when:
        def step = new Buy2ForSpecialPrice("bathingDuck", new BigDecimal(12)).calculatePrice(TEN, items)

        then:
        step.currentPrice == TEN
        step.remainingProducts == []

        where:
        items << [[], null]
    }

    def "should return a remaining list of not concerned items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(ONE, "foo", "Foo"),
                new Item(ONE, "bar", "Bar"),
        ]

        when:
        def step = new Buy2ForSpecialPrice("bathingDuck", new BigDecimal(12)).calculatePrice(ONE, items)

        then:
        step.remainingProducts.size() == 2
    }

    def "should calculate a price for 1 item"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new Buy2ForSpecialPrice("bathingDuck", new BigDecimal(12)).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(11)

    }

    def "should calculate a price for 2 items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new Buy2ForSpecialPrice("bathingDuck", new BigDecimal(12)).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(13)
    }

    def "should calculate a price for 3 items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new Buy2ForSpecialPrice("bathingDuck", new BigDecimal(12)).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(23)
    }

    def "should calculate a price for 4 items"(){
        given:
        def items = [
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck"),
                new Item(TEN, "bathingDuck", "Bathing duck")
        ]

        when:
        def step = new Buy2ForSpecialPrice("bathingDuck", new BigDecimal(12)).calculatePrice(ONE, items)

        then:
        !step.remainingProducts
        step.currentPrice == new BigDecimal(25)
    }
}

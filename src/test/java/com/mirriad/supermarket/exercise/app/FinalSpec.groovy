package com.mirriad.supermarket.exercise.app

import com.mirriad.supermarket.exercise.app.calculation.Buy2ForSpecialPrice
import com.mirriad.supermarket.exercise.app.calculation.BuySame3Pay2
import com.mirriad.supermarket.exercise.domain.Checkout
import com.mirriad.supermarket.exercise.domain.Item
import spock.lang.Specification

import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.ONE
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN
import static java.math.BigDecimal.TEN

class FinalSpec extends Specification {

    private PriceService service

    def setup(){
        service = new PriceService()
    }

    def "should get the right checkout"(){
        given: "Rules"
        def rules =[
                new BuySame3Pay2("shinyThing"),
                new Buy2ForSpecialPrice("fluffyDuck", new BigDecimal(12))
        ]

        and: "Items"
        def items = [
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(TEN, "fluffyDuck", "Fluffy yellow duck"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(new BigDecimal(100), "shinyThing", "Shiny Thing"),
                new Item(ONE, "gum", "Some gum"),
                new Item(ONE, "gum", "Some gum"),
                new Item(ONE, "gum", "Some gum")
        ]

        when:
        def checkout = service.calculatePrice(items, rules)

        then:
        checkout.totalPrice == 349
        checkout.items.size() == 14
        println checkout.toString()
    }
}

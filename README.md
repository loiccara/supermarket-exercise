### How do I get set up? ###

* clone this repository
* build with gradle
* execute the FinalSpec to see an example of the results

This project was meant to be a springboot project with a mocked h2 database, but at the end of my developments I decided to focus only on the domain and the business logic. An execution example is done in a test harness.

It is easy to setup a new Rule : just implement the CalculationRule interface. The new rule can then be used right away. However, this would need a new deployment for every new rule. There are dynamic ways to handle this problem, but leading to a hell in maintenance.

### Who do I talk to? ###

cara.loic.pro@gmail.com